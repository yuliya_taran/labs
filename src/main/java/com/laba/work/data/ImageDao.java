package com.laba.work.data;

import com.laba.work.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ImageDao extends JpaRepository<Image, Integer> {
    List<Image> findAllByOrderByViewingDesc();
    Image findById(int id);
}
