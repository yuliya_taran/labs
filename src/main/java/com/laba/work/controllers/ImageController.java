package com.laba.work.controllers;

import com.laba.work.data.ImageDao;
import com.laba.work.data.UserDao;
import com.laba.work.model.Image;
import com.laba.work.model.Message;
import com.laba.work.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
public class ImageController {
    @Autowired
    private ImageDao imageDao;

    @RequestMapping(value = "/gallery", method = RequestMethod.GET)
    public ModelAndView gallery(Model model) {
        List<Image> allImages = imageDao.findAllByOrderByViewingDesc();
        ModelAndView images = new ModelAndView("gallery", "imageList", allImages);
        model.addAttribute("username", SecurityContextHolder.getContext().getAuthentication().getName());
        return images;
    }

    @RequestMapping(value = "/showImage", method = RequestMethod.GET)
    public ModelAndView messages(@RequestParam(name="imageId") Integer imageId, Model model) {
        Image image = imageDao.findById(imageId);
        int viewings = image.getViewing();
        image.setViewing(++viewings);
        imageDao.save(image);
        model.addAttribute("image", image);
        return new ModelAndView("image", (Map<String, ?>) model);
    }

}
