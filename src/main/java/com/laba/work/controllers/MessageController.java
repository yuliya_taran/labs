package com.laba.work.controllers;

import com.laba.work.data.MessageDao;
import com.laba.work.data.UserDao;
import com.laba.work.model.Message;
import com.laba.work.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
public class MessageController {
    @Autowired
    private MessageDao messageDao;
    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public ModelAndView messages(Model model) {
        List<Message> allMessages = messageDao.findAll();
        ModelAndView messages = new ModelAndView("messages", "messageList", allMessages);
        model.addAttribute("message", new Message());
        model.addAttribute("username", SecurityContextHolder.getContext().getAuthentication().getName());
        return messages;
    }

    @RequestMapping(value = "/postMessage", method = RequestMethod.POST)
    public ModelAndView postMessage(@Valid Message message, BindingResult bindingResult, Model model) {
        User user = userDao.findByName(SecurityContextHolder.getContext().getAuthentication().getName());
        message.setUser(user);
        if (!bindingResult.hasErrors()) {
            messageDao.save(message);
            return new ModelAndView("redirect:/messages", (Map<String, ?>) model);
        }
        return new ModelAndView("messages", "messageList", messageDao.findAll());
    }

    @RequestMapping(value = "/deleteMessage", method = RequestMethod.GET)
    public ModelAndView deleteMessage(@RequestParam(name="messageId") Integer messageId, Model model) {
        messageDao.delete(messageId);
        return new ModelAndView("redirect:/messages", (Map<String, ?>) model);
    }
}
