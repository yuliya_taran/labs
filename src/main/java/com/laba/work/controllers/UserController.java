package com.laba.work.controllers;

import com.laba.work.data.MessageDao;
import com.laba.work.data.UserDao;
import com.laba.work.model.Message;
import com.laba.work.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {
    @Autowired
    private UserDao userDao;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage(Model model) {
        return "login";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView messages(Model model) {
        ModelAndView messages = new ModelAndView("registration", "user", new User());
        return messages;
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ModelAndView postMessage(@Valid User user, BindingResult bindingResult, Model model) {
        if (!bindingResult.hasErrors()) {
            userDao.save(user);
            return new ModelAndView("redirect:/messages");
        }
        return new ModelAndView("registration");
    }

}
