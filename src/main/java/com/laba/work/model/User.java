package com.laba.work.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

@Entity
public class User {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "User_id")
    private int id;

    @NotEmpty(message = "Please provide your name")
    private String name;

    @Length(min = 3, message = "Password min length is 3 characters")
    @NotEmpty(message = "Please provide your password")
    private String password;

    @Email(message = "Please provide valid email")
    @NotEmpty(message = "Please provide your email")
    private String email;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
