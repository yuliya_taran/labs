package com.laba.work.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.Valid;

@Entity
public class Image {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    @Column(name = "Image_id")
    private int id;

    @NotEmpty(message = "Please provide path")
    private String path;

    private int viewing;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getViewing() {
        return viewing;
    }

    public void setViewing(int viewing) {
        this.viewing = viewing;
    }
}
